@extends('public.layout')

@section('title')
    {{$category->name}}
@endsection
@push('meta')
    <meta name="keywords" content="{{ $category->meta_keyword }}">
    <meta name="title" content="{{ $category->meta_title ?: $category->name }}">
    <meta name="description" content="{{ $category->meta_description}}">
    <meta name="twitter:card" content="summary">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:title" content="{{ $category->meta_title ?: $category->name }}">
    <meta property="og:description" content="{{ $category->meta_description }}">
    <meta property="og:image" content="{{media_url($category->thumbnail)}}">
    <meta property="og:locale" content="{{ locale() }}">

    @foreach (supported_locale_keys() as $code)
        <meta property="og:locale:alternate" content="{{ $code }}">
    @endforeach
@endpush
@section('content')
    <section class="product-search-wrap">
        <div class="container">
            <div class="product-search">
                <div class="product-search-left">
                    @if (!empty($categories))
                        <div class="d-none d-lg-block browse-categories-wrap">
                            <h4 class="section-title">
                                {{ trans('storefront::products.browse_categories') }}
                            </h4>

                            @include('public.blogs.index.browse_categories')
                        </div>
                    @endif
                </div>

                <div class="product-search-right">
                    @if(!empty($category->thumbnail))
                    <div class="d-none d-lg-block categories-banner">
                        <img src="{{media_url($category->thumbnail)}}" alt="{{$category->name}}" title="{{$category->name}}">
                    </div>
                    @endif
                    <div class="search-result">
                        <div class="search-result-top">
                            <div class="content-left"><h4>{{$category->name}}</h4></div>
                        </div>
                        @if(!empty($data) && is_countable($data) && count($data))
                        <div class="search-result-middle">
                            <div class="grid-view-products">
                                @foreach($data as $key)
                                <div class="col">
                                    <div class="product-card">
                                        <div class="product-card-top">
                                            <a href="{{route('blogs.show',['slug'=>$key->slug])}}" class="product-image" style="height: auto">
                                                <img src="{{media_url($key->thumbnail)}}" alt="blog image" class="">
                                            </a> 
                                        </div>
                                        <div class="product-card-middle">
                                            <a href="{{route('blogs.show',['slug'=>$key->slug])}}" class="product-name">
                                                <h6>{{$key->name}}</h6>
                                            </a>
                                            <div class="product-price product-price-clone">
                                                <a href="{{route('categories.blogs.index',['category'=>$key->categories->slug])}}">{{optional($key->categories)->name}}</a>
                                            </div>
                                        </div>
                                        <div class="product-card-bottom">
                                            <div class="product-price"><a href="{{route('categories.blogs.index',['category'=>$key->categories->slug])}}">{{optional($key->categories)->name}}</a></div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            
                        </div>
                        <div class="search-result-bottom">
                            {{ $data->links() }}
                        </div>
                        @else
                        <div class="search-result-middle emptyProducts">
                            <div class="empty-message">
                                @include('public.products.index.empty_results_logo')
                                <h2>Oops! No blogs found.</h2>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
