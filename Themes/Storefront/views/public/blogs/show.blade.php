@extends('public.layout')

@section('title')
    {{$data->name}}
@endsection
@push('meta')
    <meta name="keywords" content="{{ $data->meta_keyword }}">
    <meta name="title" content="{{ $data->meta_title ?: $data->name }}">
    <meta name="description" content="{{ $data->meta_description }}">
    <meta name="twitter:card" content="summary">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:title" content="{{ $data->meta_title ?: $data->name }}">
    <meta property="og:description" content="{{ $data->meta_description }}">
    <meta property="og:image" content="{{media_url($data->thumbnail)}}">
    <meta property="og:locale" content="{{ locale() }}">

    @foreach (supported_locale_keys() as $code)
        <meta property="og:locale:alternate" content="{{ $code }}">
    @endforeach
@endpush
@section('content')
    <section class="product-search-wrap">
        <div class="container">
            <div class="product-search">
                <div class="product-search-left">
                    @if (!empty($categories))
                        <div class="d-none d-lg-block browse-categories-wrap">
                            <h4 class="section-title">
                                {{ trans('storefront::products.browse_categories') }}
                            </h4>

                            @include('public.blogs.index.browse_categories')
                        </div>
                    @endif
                </div>

                <div class="product-search-right">
                    @if(!empty($data->thumbnail))
                    <div class="d-none d-lg-block categories-banner">
                        <img src="{{media_url($data->thumbnail)}}" alt="{{$data->name}}" title="{{$data->name}}">
                    </div>
                    @endif
                    <div class="search-result">
                        <h1>{{$data->name}}</h1>
                        <div id="description" class="tab-pane description">
                            {!! $data->description !!}
                        </div>
                        <div class="product-details-info" style="padding-left: 0px">
                            <div class="details-info-bottom">
                                <ul class="list-inline additional-info">
                                    @if (!empty($data->categories))
                                        <li>
                                            <label>{{ trans('storefront::product.categories') }}</label>
                                                <a href="{{ $data->categories->url() }}">{{ $data->categories->name }}</a>
                                        </li>
                                    @endif

                                    @if ($data->tags->isNotEmpty())
                                        <li>
                                            <label>{{ trans('storefront::product.tags') }}</label>

                                            @foreach ($data->tags as $tag)
                                                <a href="{{ $tag->url() }}">{{ $tag->name }}</a>{{ $loop->last ? '' : ',' }}
                                            @endforeach
                                        </li>
                                    @endif
                                </ul>

                                @include('public.blogs.show.social_share')
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
