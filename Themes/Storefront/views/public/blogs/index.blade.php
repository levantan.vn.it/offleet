@extends('public.layout')

@section('title')
    Blogs
@endsection
@section('content')
    <section class="product-search-wrap">
        <div class="container">
            <div class="product-search">
                <div class="product-search-left">
                    @if (!empty($categories))
                        <div class="d-none d-lg-block browse-categories-wrap">
                            <h4 class="section-title">
                                {{ trans('storefront::products.browse_categories') }}
                            </h4>

                            @include('public.blogs.index.browse_categories')
                        </div>
                    @endif
                </div>

                <div class="product-search-right">
                    <div class="search-result">
                        @if(!empty($data) && is_countable($data) && count($data))
                        <div class="search-result-middle">
                            <div class="grid-view-products">
                                @foreach($data as $key)
                                <div class="col">
                                    <div class="product-card">
                                        <div class="product-card-top">
                                            <a href="{{route('blogs.show',['slug'=>$key->slug])}}" class="product-image" style="height: auto">
                                                <img src="{{media_url($key->thumbnail)}}" alt="blog image" class="">
                                            </a> 
                                        </div>
                                        <div class="product-card-middle">
                                            <a href="{{route('blogs.show',['slug'=>$key->slug])}}" class="product-name">
                                                <h6>{{$key->name}}</h6>
                                            </a>
                                            <div class="product-price product-price-clone">
                                                <a href="{{route('categories.blogs.index',['category'=>$key->categories->slug])}}">{{optional($key->categories)->name}}</a>
                                            </div>
                                        </div>
                                        <div class="product-card-bottom">
                                            <div class="product-price"><a href="{{route('categories.blogs.index',['category'=>$key->categories->slug])}}">{{optional($key->categories)->name}}</a></div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            
                        </div>
                        <div class="search-result-bottom">
                            {{ $data->links() }}
                        </div>
                        @else
                        <div class="search-result-middle emptyProducts">
                            <div class="empty-message">
                                @include('public.products.index.empty_results_logo')
                                <h2>Oops! No blogs found.</h2>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
