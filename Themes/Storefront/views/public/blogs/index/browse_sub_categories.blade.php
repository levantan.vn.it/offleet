<ul>
    @foreach ($subCategories as $subCategory)
        <li class="{{!empty($category) && $category->id = $subCategory->id?'active':''}} {{$subCategory->fk_childs->isNotEmpty()?'parent':''}}">
            <a href="{{ route('categories.blogs.index', ['category' => $subCategory->slug]) }}">
                {{ $subCategory->name }}
            </a>

            @if ($subCategory->fk_childs->isNotEmpty())
                @include('public.blogs.index.browse_sub_categories', ['subCategories' => $subCategory->fk_childs])
            @endif
        </li>
    @endforeach
</ul>
