<ul class="list-inline browse-categories">
    @foreach ($categories as $cate)
        <li class="{{!empty($category) && $category->id == $cate->id?'active':''}} {{$cate->fk_childs->isNotEmpty()?'parent':''}}">
            
            <a href="{{ route('categories.blogs.index', ['category' => $cate->slug]) }}">
                {{ $cate->name }}
            </a>

            @if ($cate->fk_childs->isNotEmpty())
                @include('public.blogs.index.browse_sub_categories', ['subCategories' => $cate->fk_childs])
            @endif
        </li>
    @endforeach
</ul>
