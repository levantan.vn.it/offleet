<?php

return [
    'blog' => 'blog',
    'blogs' => 'blogs',
    'table' => [
        'thumbnail' => 'Thumbnail',
        'name' => 'Name',
        'price' => 'Price',
    ],
    'tabs' => [
        'group' => [
            'basic_information' => 'Basic Information',
            'advanced_information' => 'Advanced Information',
        ],
        'general' => 'General',
        'price' => 'Price',
        'inventory' => 'Inventory',
        'images' => 'Images',
        'seo' => 'SEO',
        'related_blogs' => 'Related blogs',
        'up_sells' => 'Up-Sells',
        'cross_sells' => 'Cross-Sells',
        'additional' => 'Additional',
    ],
    'form' => [
        'enable_the_blog' => 'Enable the blog',
        'price_types' => [
            'fixed' => 'Fixed',
            'percent' => 'Percent',
        ],
        'manage_stock_states' => [
            '0' => 'Don\'t Track Inventory',
            '1' => 'Track Inventory',
        ],
        'stock_availability_states' => [
            '1' => 'In Stock',
            '0' => 'Out of Stock',
        ],
        'base_image' => 'Base Image',
        'additional_images' => 'Additional Images',
    ],
];
