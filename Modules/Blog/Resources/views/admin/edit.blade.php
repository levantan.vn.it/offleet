@extends('admin::layout')
@push('styles')
    {{-- JQuery --}}
    <script src="{{ asset('libs/jquery/jquery-3.2.1.min.js') }}"></script>

    {{-- Toastr Notification JS --}}
    <script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.config.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/toastr/2.1.3/toastr.min.css') }}">
    
    {{-- Google Jquery Library --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{ asset('libs/custom/custom.css') }}">
    <script src="{{ asset('js/main.handler.js') }}"></script>
@endpush
@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => "Blogs"]))
    @slot('subtitle', $data->name)

    <li><a href="{{ route('admin.blogs.index') }}">Blogs</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => "Blogs"]) }}</li>
@endcomponent

@section('content')
    <form action="{{ route('admin.blogs.update', $data->id) }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@method('PUT')
		@csrf
		<div class="row">
			<div class="col-lg-12">
				<div class="box" style="font-size: 15px;">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Name <span class="required">*</span></label>
							<input type="text" name="name" value="{{ $data->name }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Slug <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">{{ url('') }}</span>
								<input type="text" name="slug" readonly="" value="{{ $data->slug }}" class="form-control" placeholder="category-blog" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,255}$" required>
								<span class="input-group-btn">
									<button class="btn btn-info spinner-refresh" type="button" onclick="slug_make(event);"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
							<label id="slug-error" class="error" for="slug" style="display: none"></label>
						</div>
						{{ Form::wysiwyg('description', "Description",$errors, $data, ['labelCol' => 2, 'required' => true]) }}

						<div class="form-group">
							<label for="parent">Category <span class="required">*</span></label>
							<select name="category_id" class="custom-select form-control">
								<option value=""></option>
								@foreach($categories as $category_1)
									<option value="{{ $category_1->id }}"{!! $data->category_id == $category_1->id ? ' selected' : null !!}>{{ $category_1->name }}</option>
								@endforeach
							</select>
						</div>
						<hr>
						<div class="form-group">
							<label>Image</label>
							<div class="custom-image-upload custom-iu-md image-preview m-0">
								<img class="img-preview" src="{{ media_url($data->thumbnail) }}">
								<label class="custom-label">
									<input type="file" name="file" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="custom-toggle toggle-info">
								<input type="checkbox" value="1" name="is_active" class="toggle-checkbox"{!! $data->is_active ? ' checked' : null !!}>
								<div class="toggle-inner">
									<span class="toggle-button"></span>
								</div>
								<span class="toggle-text">Show</span>
							</label>
						</div>
						<hr>
						<div class="form-group">
							<label for="name">Keywords</label>
							<input type="text" name="meta_keyword" value="{{ $data->meta_keyword }}" class="form-control">
						</div>
						<div class="form-group">
							<label for="name">Meta title</label>
							<input type="text" name="meta_title" value="{{ $data->meta_title }}" class="form-control">
						</div>
						<div class="form-group">
							<label for="name">Meta description</label>
							<textarea name="meta_description" class="form-control" rows="4">{{ $data->meta_description }}</textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-primary">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection

@push('scripts')
    <script>
    	var slug_make = function(event)
    	{
    		var value = $("form").find("input[name=name]").val();
    		$("form").find("input[name=slug]").val(convertToSlug(value));
    	}
    </script>
@endpush
