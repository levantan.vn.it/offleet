<?php

namespace Modules\Blog\Entities;


use Modules\Support\Eloquent\Model;
use Modules\Media\Eloquent\HasMedia;
use Modules\Support\Search\Searchable;
use Modules\CategoryBlog\Entities\CategoryBlog;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Tag\Entities\Tag;

class Blog extends Model
{
    use Translatable,
        Searchable,
        Sluggable,
        HasMedia,
        SoftDeletes;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    protected $fillable = ['category_id', 'slug', 'is_active','thumbnail','meta_keyword','meta_description','meta_title'];
    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = ['name', 'description'];

    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    public function categories()
    {
        return $this->belongsTo(CategoryBlog::class, 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'blog_tags');
    }

    public function url()
    {
        return route('blogs.show', ['slug' => $this->slug]);
    }

    public static function findBySlug($slug)
    {
        return self::with([
            'categories'
        ])
        ->where('slug', $slug)
        ->firstOrFail();
    }

    public function clean()
    {
        return array_except($this->toArray(), [
            'description',
            'short_description',
            'translations',
            'category_id',
            'viewed',
            'created_at',
            'updated_at',
            'deleted_at',
        ]);
    }

    /**
     * Get table data for the resource
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table($request)
    {
        $query = $this->newQuery()
            ->withoutGlobalScope('active')
            ->withName()
            ->withBaseImage()
            ->withPrice()
            ->addSelect(['id', 'is_active', 'created_at'])
            ->when($request->has('except'), function ($query) use ($request) {
                $query->whereNotIn('id', explode(',', $request->except));
            });

        return new ProductTable($query);
    }

    /**
     * Get the indexable data array for the product.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        // MySQL Full-Text search handles indexing automatically.
        if (config('scout.driver') === 'mysql') {
            return [];
        }

        $translations = $this->translations()
            ->withoutGlobalScope('locale')
            ->get(['name', 'description', 'short_description']);

        return ['id' => $this->id, 'translations' => $translations];
    }

    public function searchTable()
    {
        return 'blog_translations';
    }

    public function searchKey()
    {
        return 'blog_id';
    }

    public function searchColumns()
    {
        return ['name'];
    }
}
