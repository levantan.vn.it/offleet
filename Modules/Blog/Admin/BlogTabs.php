<?php

namespace Modules\Blog\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;

class BlogTabs extends Tabs
{
    public function make()
    {
        $this->group('blog_information', "Blog information")
            ->active()
            ->add($this->general())
            ->add($this->seo());
    }

    private function general()
    {
        return tap(new Tab('general', "General"), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);
            $tab->fields(['name', 'short_description','description', 'is_active', 'slug']);
            $tab->view('blog::admin.blogs.tabs.general');
        });
    }

    private function seo()
    {
        return tap(new Tab('seo', "Seo"), function (Tab $tab) {
            $tab->weight(10);
            $tab->view('blog::admin.blogs.tabs.seo');
        });
    }
}
