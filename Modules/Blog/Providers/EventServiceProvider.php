<?php

namespace Modules\Blog\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \Modules\Blog\Events\BlogViewed::class => [
            \Modules\Blog\Listeners\IncrementBlogView::class,
            \Modules\Blog\Listeners\AddToRecentlyViewed::class,
        ],
        \Modules\Blog\Events\ShowingBlogList::class => [
            \Modules\Blog\Listeners\StoreSearchTerm::class,
        ],
    ];
}
