<?php

use Illuminate\Support\Facades\Route;

Route::get('blogs', [
    'as' => 'admin.blogs.index',
    'uses' => 'BlogController@index',
]);

Route::get('blogs/create', [
    'as' => 'admin.blogs.create',
    'uses' => 'BlogController@create',
]);

Route::post('blogs', [
    'as' => 'admin.blogs.store',
    'uses' => 'BlogController@store',
]);

Route::get('blogs/{id}/edit', [
    'as' => 'admin.blogs.edit',
    'uses' => 'BlogController@edit',
]);

Route::put('blogs/{id}', [
    'as' => 'admin.blogs.update',
    'uses' => 'BlogController@update',
]);

Route::delete('blogs/{ids}', [
    'as' => 'admin.blogs.destroy',
    'uses' => 'BlogController@destroy',
]);
