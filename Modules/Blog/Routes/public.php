<?php

use Illuminate\Support\Facades\Route;

Route::get('blogs', 'BlogController@index')->name('blogs.index');
Route::get('blogs/{slug}', 'BlogController@show')->name('blogs.show');

Route::get('suggestions', 'SuggestionController@index')->name('suggestions.index');

Route::post('blogs/{id}/price', 'blogPriceController@show')->name('blogs.price.show');
