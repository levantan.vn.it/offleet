<?php

namespace Modules\Blog\Sidebar;

use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item("Blogs", function (Item $item) {
                $item->item("List", function (Item $item) {
                    $item->weight(5);
                    $item->route('admin.blogs.index');
                });
                $item->item(trans('category::sidebar.categories'), function (Item $item) {
                    $item->weight(10);
                    $item->route('admin.categoryblog.index');
                });
                $item->icon('fa fa-rss');
                $item->weight(10);
                $item->route('admin.blogs.index');
            });
        });
    }
}
