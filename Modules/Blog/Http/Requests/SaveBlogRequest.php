<?php

namespace Modules\Blog\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Blog\Entities\Blog;
use Modules\Core\Http\Requests\Request;

class SaveBlogRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => $this->getSlugRules(),
            'name' => 'required',
            'description' => 'required',
            'category_id' => ['required', Rule::exists('categoryblog', 'id')],
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.blogs.update'
            ? ['required']
            : ['sometimes'];

        $slug = BLog::withoutGlobalScope('active')->where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('blogs', 'slug')->ignore($slug, 'slug');

        return $rules;
    }
}
