<?php

namespace Modules\Blog\Http\Response;

use Illuminate\Support\Collection;
use Modules\Blog\Entities\Blog;
use Modules\CategoryBlog\Entities\CategoryBlog;
use Illuminate\Contracts\Support\Responsable;

class SuggestionsResponse implements Responsable
{
    private $query;
    private $blogs;
    private $categories;
    private $totalResults;

    /**
     * Create a new instance.
     *
     * @param string $query
     * @param int $totalResults
     * @param \Illuminate\Support\Collection $blogs
     * @param \Illuminate\Support\Collection $categories
     */
    public function __construct($query, Collection $blogs, Collection $categories, $totalResults)
    {
        $this->query = $query;
        $this->blogs = $blogs;
        $this->categories = $categories;
        $this->totalResults = $totalResults;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'categories' => $this->transformCategories(),
            'blogs' => $this->transformblogs(),
            'remaining' => $this->getRemainingCount(),
        ]);
    }

    /**
     * Transform the categories.
     *
     * @return \Illuminate\Support\Collection
     */
    private function transformCategories()
    {
        return $this->categories->map(function (Category $category) {
            return [
                'slug' => $category->slug,
                'name' => $category->name,
                'url' => $category->url(),
            ];
        })->unique('slug')->values();
    }

    /**
     * Transform the blogs.
     *
     * @return \Illuminate\Support\Collection
     */
    private function transformblogs()
    {
        return $this->blogs->map(function (blog $blog) {
            return [
                'slug' => $blog->slug,
                'name' => $this->highlight($blog->name),
                'formatted_price' => $blog->getFormattedPriceAttribute(),
                'base_image' => $blog->getBaseImageAttribute(),
                'is_out_of_stock' => $blog->isOutOfStock(),
                'url' => $blog->url(),
            ];
        });
    }

    /**
     * Highlight the given text.
     *
     * @param string $text
     * @return string
     */
    private function highlight($text)
    {
        $query = str_replace(' ', '|', preg_quote($this->query));

        return preg_replace("/($query)/i", '<em>$1</em>', $text);
    }

    /**
     * Get remaining results count.
     *
     * @return int
     */
    private function getRemainingCount()
    {
        return $this->totalResults - $this->blogs->count();
    }
}
