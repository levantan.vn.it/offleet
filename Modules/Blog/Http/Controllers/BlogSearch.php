<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Modules\Blog\Entities\Blog;
use Modules\Category\Entities\Category;
use Modules\Attribute\Entities\Attribute;

trait BlogSearch
{
    /**
     * Search products for the request.
     *
     * @param \Modules\Product\Entities\Product $model
     * @param \Modules\Product\Filters\ProductFilter $productFilter
     * @return \Illuminate\Http\Response
     */
    public function searchBlogs(Blog $model)
    {

        if (request()->filled('query')) {
            $model = $model->search(request('query'));
        }

        if (request()->filled('category')) {
            $model = $model->where('category_id',request('category'));
        }

        $blogs = $model->paginate(request('perPage', 30));

        return response()->json([
            'blogs' => $blogs
        ]);
    }
}
