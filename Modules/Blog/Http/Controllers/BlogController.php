<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\Blog;
use Modules\CategoryBlog\Entities\CategoryBlog;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = Blog::where('is_active',1)->orderBy('created_at', 'desc')->paginate(40);
        $categories = CategoryBlog::where('is_active',1)->whereNull('parent_id')->get();
        return view('public.blogs.index',compact('data','categories'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($slug)
    {
        $data = Blog::where('slug',$slug)->where('is_active',1)->first();
        if(empty($data)){
            return redirect(url('404'));
        }
        $categories = CategoryBlog::where('is_active',1)->whereNull('parent_id')->get();
        return view('public.blogs.show', compact('data','categories'));
    }
}
