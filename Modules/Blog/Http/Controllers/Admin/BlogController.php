<?php

namespace Modules\Blog\Http\Controllers\Admin;

use Modules\Blog\Entities\Blog;
use Modules\CategoryBlog\Entities\CategoryBlog;
use Modules\Blog\Http\Requests\SaveBlogRequest;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\DB;
class BlogController
{
    public function index(){

        $data = Blog::orderBy('created_at', 'desc')->get();
        return view('blog::admin.index', compact('data'));
    }
    public function create()
    {
        $categories = CategoryBlog::all();
        return view('blog::admin.create', compact('categories'));
    }

    public function store(SaveBlogRequest $request)
    {

        /** Upload Thumbnail */
        $thumbnail = null;
        if($request->hasFile('file')) {
            $thumbnail = $request->file('file')->store('blogs','public_app');
            $request->merge(['thumbnail' => $thumbnail]);
        }
        $is_active = $request->has('is_active') ? true : false;
        $request->merge(['is_active' => $is_active]);
        $data = new Blog;
        $data = $data->create($request->all());

        return redirect()->route('admin.blogs.index')->with([
            'success'   =>  'Added blog successfull.'
        ]);
    }

    public function edit($id)
    {
        $data = Blog::findOrFail($id);
        $categories = CategoryBlog::all();
        return view('blog::admin.edit', compact('data', 'categories'));
    }

    public function update(SaveBlogRequest $request, $id)
    {
        $data = Blog::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if($request->hasFile('file')) {
            /** Xóa thumbnail cũ */
            if(File::exists(public_path('media/'.$data->thumbnail))) File::delete(public_path('media/'.$data->thumbnail));
            /** Upload thumbnail mới */
            $thumbnail = $request->file('file')->store('blogs','public_app');
            $request->merge(['thumbnail' => $thumbnail]);
        }
        $is_active = $request->has('is_active') ? true : false;
        $request->merge(['is_active' => $is_active]);
        $data = $data->update($request->all());

        return redirect()->route('admin.blogs.index')->with([
            'success'   =>  'Updated blog successfull.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $data = Blog::findOrFail($id);
            if(!empty($data)){
                if(File::exists(public_path('media/'.$data->thumbnail))) File::delete(public_path('media/'.$data->thumbnail));
            }
            Blog::withoutGlobalScope('active')
            ->findOrFail($id)
            ->delete();
            \Session::flash('success', 'Deleted blog success');
            return response()->json([
                'alert' => 'success',
                'title' => 'Completed',
                'msg' => 'Deleted one item.'
            ]);
        }
    }
}
