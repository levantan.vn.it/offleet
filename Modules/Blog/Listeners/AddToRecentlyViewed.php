<?php

namespace Modules\Blog\Listeners;

use Exception;
use Modules\Blog\RecentlyViewed;
use Modules\Blog\Events\BlogViewed;

class AddToRecentlyViewed
{
    /**
     * The recently viewed instance.
     *
     * @var \Modules\Product\RecentlyViewed
     */
    private $recentlyViewed;

    /**
     * Create a new event listener.
     *
     * @param \Modules\Product\RecentlyViewed $recentlyViewed
     */
    public function __construct(RecentlyViewed $recentlyViewed)
    {
        $this->recentlyViewed = $recentlyViewed;
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Product\Events\ProductViewed $event
     * @return void
     */
    public function handle(BlogViewed $event)
    {
        try {
            $this->recentlyViewed->store($event->product);
        } catch (Exception $e) {
            //
        }
    }
}
