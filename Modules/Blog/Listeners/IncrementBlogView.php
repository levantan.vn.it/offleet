<?php

namespace Modules\Blog\Listeners;

use Modules\Blog\Events\BlogViewed;

class IncrementProductView
{
    /**
     * Handle the event.
     *
     * @param \Modules\Product\Events\ProductViewed $event
     * @return void
     */
    public function handle(BlogViewed $event)
    {
        $event->blog->increment('viewed');
    }
}
