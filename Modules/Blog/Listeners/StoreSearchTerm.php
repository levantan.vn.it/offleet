<?php

namespace Modules\Blog\Listeners;

use Modules\Blog\Entities\SearchTerm;
use Modules\Blog\Events\ShowingBlogList;

class StoreSearchTerm
{
    /**
     * Handle the event.
     *
     * @param \Modules\Product\Events\ShowingProductList $event
     * @return void
     */
    public function handle(ShowingBlogList $event)
    {
        if (! request()->filled('query')) {
            return;
        }

        SearchTerm::updateOrCreate(
            ['term' => request('query')],
            ['results' => $event->blogs->count()]
        )->increment('hits');
    }
}
