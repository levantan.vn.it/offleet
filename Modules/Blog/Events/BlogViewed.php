<?php

namespace Modules\Blog\Events;

use Illuminate\Queue\SerializesModels;

class BlogViewed
{
    use SerializesModels;

    /**
     * The product entity.
     *
     * @var \Modules\Product\Entities\Product
     */
    public $blog;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($blog)
    {
        $this->blog = $blog;
    }
}
