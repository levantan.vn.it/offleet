<?php

namespace Modules\Blog\Events;

use Illuminate\Queue\SerializesModels;

class ShowingBlogList
{
    use SerializesModels;

    /**
     * Collection of product.
     *
     * @var \Illuminate\Database\Eloquent\Collection
     */
    public $blogs;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($blogs)
    {
        $this->blogs = $blogs;
    }
}
