<?php

namespace Modules\CategoryBlog\Sidebar;

use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    public function extend(Menu $menu)
    {
        
        // $menu->group(trans('admin::sidebar.content'), function (Group $group) {
        //     $group->item("Category Blog", function (Item $item) {
        //         $item->icon('fa fa-newspaper-o');
        //         $item->weight(10);
        //         $item->route('admin.categoryblog.index');
        //     });
        // });
    }
}
