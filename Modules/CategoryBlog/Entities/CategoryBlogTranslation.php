<?php

namespace Modules\CategoryBlog\Entities;

use Modules\Support\Eloquent\TranslationModel;

class CategoryBlogTranslation extends TranslationModel
{
	protected $table = 'categoryblog_translations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
