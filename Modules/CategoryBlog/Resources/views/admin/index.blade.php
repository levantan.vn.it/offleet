@extends('admin::layout')
@push('styles')
    {{-- JQuery --}}
    <script src="{{ asset('libs/jquery/jquery-3.2.1.min.js') }}"></script>
    
    {{-- Google Jquery Library --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- Bootstrap --}}  
    <script src="{{ asset('libs/popper/1.14.3/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/bootstrap/4-4.0.0-beta/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
    <script src="{{ asset('libs/datatable/1.10.16/datatables.config.js?v=123') }}"></script>
    <script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>

    {{-- Toastr Notification JS --}}
    <script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.config.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/toastr/2.1.3/toastr.min.css') }}">
    {{-- Sweet Alert --}}
    <script src="{{ asset('libs/sweetalert/2v7.0.9/sweetalert.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/sweetalert/2v7.0.9/sweetalert.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/custom/custom.css') }}">
@endpush
@component('admin::components.page.header')
    @slot('title', trans('category::categories.categories'))

    <li class="active">{{ trans('category::categories.categories') }}</li>
@endcomponent


@section('content')
    <div class="box box-default" style="font-size: 15px">
        <div class="box-body clearfix">
            <div class="row">
                 <div class="col-md-12">
                     <div class="btn-group pull-right">
                         <a href="{{route('admin.categoryblog.create')}}" class="btn btn-primary btn-actions btn-create" style="font-size: 15px">
                         Create New
                         </a>
                     </div>
                 </div>   
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="dataTable" style="font-size: 15px">
                    <thead>
                        <tr>
                            <th class="no-sort" width="50"></th>
                            <th>Name</th>
                            <th>Url</th>
                            <th>Show</th>
                            <th class="no-sort" width="50"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)            
                            <tr>
                                <td>
                                    <img src="{{ media_url($item->thumbnail) }}" width="60">
                                </td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>{{ $item->slug }}</td>
                                <td>
                                    {{ $item->is_active?'Yes':'No' }}
                                </td>
                                <td>
                                    <ul class="table-options">
                                        <li>
                                            <a href="{{ route('admin.categoryblog.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                        </li>
                                        <li>
                                            <button type="button" data-toggle="tooltip" data-placement="top" title="Delete" onclick="destroyItem(event, '{{ route('admin.categoryblog.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            @foreach($item->fk_childs as $child)
                                <tr>
                                    <td>
                                        <img src="{{ media_url($child->thumbnail) }}" width="60">
                                    </td>
                                    <td>
                                        — {{ $child->name }}
                                    </td>
                                    <td>{{ $child->slug }}</td>
                                    <td>
                                        {{ $child->is_active?'Yes':'No' }}
                                    </td>
                                    <td>
                                        <ul class="table-options">
                                            <li>
                                                <a href="{{ route('admin.categoryblog.edit', $child->id) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                            </li>
                                            <li>
                                                <button type="button" data-toggle="tooltip" data-placement="top" title="Delete" onclick="destroyItem(event, '{{ route('admin.categoryblog.destroy', $child->id) }}')"><i class="fa fa-remove"></i></button>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>

                                @foreach($child->fk_childs as $grandchild)
                                    <tr>
                                        <td>
                                            <img src="{{ media_url($grandchild->thumbnail) }}" width="60">
                                        </td>
                                        <td>
                                            ——— {{ $grandchild->name }}
                                        </td>
                                        <td>{{ $grandchild->slug }}</td>
                                        <td>
                                            {{ $grandchild->is_active?'Yes':'No' }}
                                        </td>
                                        <td>
                                            <ul class="table-options">
                                                <li>
                                                    <a href="{{ route('admin.categoryblog.edit', $grandchild->id) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </li>
                                                <li>
                                                    <button type="button" data-toggle="tooltip" data-placement="top" title="Delete" onclick="destroyItem(event, '{{ route('admin.categoryblog.destroy', $grandchild->id) }}')"><i class="fa fa-remove"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
            
        </div>

        <div class="overlay loader hide">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/admin.resource.handler.js?v='.time()) }}"></script>
@endpush
