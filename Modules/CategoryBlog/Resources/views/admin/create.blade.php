@extends('admin::layout')
@push('styles')
    {{-- JQuery --}}
    <script src="{{ asset('libs/jquery/jquery-3.2.1.min.js') }}"></script>
    
    {{-- Google Jquery Library --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- Toastr Notification JS --}}
    <script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.config.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/toastr/2.1.3/toastr.min.css') }}">

    <link rel="stylesheet" href="{{ asset('libs/custom/custom.css') }}">
    <script src="{{ asset('js/main.handler.js') }}"></script>
@endpush
@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => "CategoryBlog"]))

    <li><a href="{{ route('admin.categoryblog.index') }}">CategoryBlog</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => "CategoryBlog"]) }}</li>
@endcomponent

@section('content')
    <form action="{{ route('admin.categoryblog.store') }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@csrf
		<div class="row" style="font-size: 15px;">
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Name <span class="required">*</span></label>
							<input type="text" name="name" value="{{ old('name') }}" class="form-control" onkeyup="slug_make(event);" required>
						</div>
						<div class="form-group">
							<label>Slug <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">{{ url('') }}</span>
								<input type="text" name="slug" value="{{ old('slug') }}" class="form-control" placeholder="category-blog" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,255}$" required>
								<span class="input-group-btn">
									<button class="btn btn-info spinner-refresh" type="button" onclick="slug_make(event);"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
							<label id="slug-error" class="error" for="slug" style="display: none"></label>
						</div>
						<script>
							$(".spinner-refresh").click(function() {
								var event = $(this);
								$(this).find("i").addClass("fa-spin");
								setTimeout(function() {
									$(event).find("i").removeClass("fa-spin");
								}, 1000);
							});
						</script>
						<div class="form-group">
							<label for="parent">Parent</label>
							<select name="parent_id" class="custom-select form-control">
								<option value=""></option>
								@foreach($categories as $category_1)
									<option value="{{ $category_1->id }}">{{ $category_1->name }}</option>
									@foreach($category_1->fk_childs as $category_2)
										<option value="{{ $category_2->id }}">— {{ $category_2->name }}</option>
									@endforeach
								@endforeach
							</select>
						</div>
						<hr>
						<div class="form-group">
							<label>Image</label>
							<div class="custom-image-upload custom-iu-md image-preview m-0">
								<img class="img-preview">
								<label class="custom-label">
									<input type="file" name="file" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="custom-toggle toggle-info">
								<input type="checkbox" value="1" name="is_active" checked class="toggle-checkbox">
								<div class="toggle-inner">
									<span class="toggle-button"></span>
								</div>
								<span class="toggle-text">Show</span>
							</label>
						</div>
						<div class="form-group">
							<button class="btn btn-primary">Submit</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="box">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Keywords</label>
							<input type="text" name="meta_keyword" value="{{ old('meta_keyword') }}" class="form-control">
						</div>
						<div class="form-group">
							<label for="name">Meta title</label>
							<input type="text" name="meta_title" value="{{ old('meta_title') }}" class="form-control">
						</div>
						<div class="form-group">
							<label for="name">Meta description</label>
							<textarea name="meta_description" class="form-control" rows="4">{{ old('meta_description') }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection

@push('scripts')
    <script>
    	var slug_make = function(event)
    	{
    		var value = $("form").find("input[name=name]").val();
    		$("form").find("input[name=slug]").val(convertToSlug(value));
    	}
    </script>
@endpush
