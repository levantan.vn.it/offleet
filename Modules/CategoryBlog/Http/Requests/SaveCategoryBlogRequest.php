<?php

namespace Modules\CategoryBlog\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\CategoryBlog\Entities\CategoryBlog;
use Modules\Core\Http\Requests\Request;

class SaveCategoryBlogRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'category::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | max:100',
            'slug' => $this->getSlugRules()
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.categoryblog.update'
            ? ['required']
            : ['nullable'];

        $slug = CategoryBlog::where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('categoryblog', 'slug')->ignore($slug, 'slug');

        return $rules;
    }
}
