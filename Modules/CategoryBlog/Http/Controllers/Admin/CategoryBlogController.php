<?php

namespace Modules\CategoryBlog\Http\Controllers\Admin;

use Modules\CategoryBlog\Entities\CategoryBlog;
use Modules\Admin\Traits\HasCrudActions;
use Modules\CategoryBlog\Http\Requests\SaveCategoryBlogRequest;
use Illuminate\Http\Request;
use File;
class CategoryBlogController
{

    public function index(){
        $data = CategoryBlog::whereNull('parent_id')->get();
        return view('categoryblog::admin.index', compact('data'));
    }
    public function create()
    {
        $categories = CategoryBlog::whereNull('parent_id')->get();
        return view('categoryblog::admin.create', compact('categories'));
    }

    public function store(SaveCategoryBlogRequest $request)
    {

        /** Upload Thumbnail */
        $thumbnail = null;
        if($request->hasFile('file')) {
            $thumbnail = $request->file('file')->store('categoryblog','public_app');
            $request->merge(['thumbnail' => $thumbnail]);
        }
        $is_active = $request->has('is_active') ? true : false;
        $request->merge(['is_active' => $is_active]);
        $data = new CategoryBlog;
        $data = $data->create($request->all());

        return redirect()->route('admin.categoryblog.index')->with([
            'success'   =>  'Added blog category successfull.'
        ]);
    }

    public function edit($id)
    {
        $data = CategoryBlog::findOrFail($id);
        $categories = CategoryBlog::whereNull('parent_id')->where('id', '!=', $id)->get();
        return view('categoryblog::admin.edit', compact('data', 'categories'));
    }

    public function update(SaveCategoryBlogRequest $request, $id)
    {
        $data = CategoryBlog::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if($request->hasFile('file')) {
            /** Xóa thumbnail cũ */
            if(File::exists(public_path('media/'.$data->thumbnail))) File::delete(public_path('media/'.$data->thumbnail));
            /** Upload thumbnail mới */
            $thumbnail = $request->file('file')->store('categoryblog','public_app');
            $request->merge(['thumbnail' => $thumbnail]);
        }
        $is_active = $request->has('is_active') ? true : false;
        $request->merge(['is_active' => $is_active]);
        $data = $data->update($request->all());

        return redirect()->route('admin.categoryblog.index')->with([
            'success'   =>  'Updated blog category successfull.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $data = CategoryBlog::find($id);
            if(!empty($data)){
                if(File::exists(public_path('media/'.$data->thumbnail))) File::delete(public_path('media/'.$data->thumbnail));
            }
            CategoryBlog::withoutGlobalScope('active')
            ->findOrFail($id)
            ->delete();
            \Session::flash('success', 'Deleted blog category success');
            return response()->json([
                'alert' => 'success',
                'title' => 'Completed',
                'msg' => 'Deleted one item.'
            ]);
        }
    }
}
