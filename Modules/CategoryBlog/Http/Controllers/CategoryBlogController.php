<?php

namespace Modules\CategoryBlog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\Blog;
use Modules\CategoryBlog\Entities\CategoryBlog;
class CategoryBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('categoryblog::index');
    }

    public function blogs($slug)
    {
        $category = CategoryBlog::where('slug',$slug)->where('is_active',1)->first();
        if(empty($category)){
            return redirect(url('404'));
        }
        $categories = CategoryBlog::where('is_active',1)->whereNull('parent_id')->get();
        $data = Blog::where('is_active',1)->where('category_id',$category->id)->paginate(40);
        return view('public.blogs.categorydetail', compact('data','categories','category'));
    }
}
