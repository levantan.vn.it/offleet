<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryBlogTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoryblog_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_blog_id')->unsigned();
            $table->string('locale');
            $table->string('name');
            $table->unique(['category_blog_id', 'locale']);
            $table->foreign('category_blog_id')->references('id')->on('categoryblog')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoryblog_translations');
    }
}
