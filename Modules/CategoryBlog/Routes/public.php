<?php

use Illuminate\Support\Facades\Route;

Route::get('category-blog', 'CategoryBlogController@index')->name('category.index');

Route::get('categories/{category}/blogs', 'CategoryBlogController@blogs')->name('categories.blogs.index');
