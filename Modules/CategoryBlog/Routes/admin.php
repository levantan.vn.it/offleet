<?php

use Illuminate\Support\Facades\Route;

Route::get('categoryblog', [
    'as' => 'admin.categoryblog.index',
    'uses' => 'CategoryBlogController@index',
]);

Route::get('categoryblog/create', [
    'as' => 'admin.categoryblog.create',
    'uses' => 'CategoryBlogController@create',
]);

Route::post('categoryblog', [
    'as' => 'admin.categoryblog.store',
    'uses' => 'CategoryBlogController@store',
    // 'middleware' => 'can:admin.categoryblog.create',
]);

Route::get('categoryblog/{id}/edit', [
    'as' => 'admin.categoryblog.edit',
    'uses' => 'CategoryBlogController@edit',
    // 'middleware' => 'can:admin.categoryblog.edit',
]);

Route::put('categoryblog/{id}', [
    'as' => 'admin.categoryblog.update',
    'uses' => 'CategoryBlogController@update',
    // 'middleware' => 'can:admin.categoryblog.edit',
]);

Route::delete('categoryblog/{ids}', [
    'as' => 'admin.categoryblog.destroy',
    'uses' => 'CategoryBlogController@destroy',
    // 'middleware' => 'can:admin.categoryblog.destroy',
]);
